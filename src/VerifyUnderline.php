<?php
class MyTest extends PHPUnit_Framework_TestCase
{

	protected $webDriver;

	public function setUp()
	{
		$capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'chrome');
		$this->webDriver = RemoteWebDriver::create("http://localhost:4444/wd/hub", $capabilities);
		$this->webDriver->manage()->window()->maximize();
	}

	public function verifyWorldMenu()
	{
		$this->webDriver->get("https://www.theweathernetwork.com/ca");
		sleep(20);
		$seeAllNewsBtn = $this->webDriver->findElement(WebDriverBy::linkText("See All News"));
		$seeAllNewsBtn->click();
		sleep(20);


		$action = new WebDriverActions($this->webDriver);
		$action->moveToElement($latestLink)->click($e2)->perform();
		$worldLink = $this->webDriver->findElement(WebDriverBy::linkText("World"));
		echo "Before Moving Cursor".$worldLink->getCssValue("text-decoration");
		$action->moveToElement($worldLink).perform();
		echo "After Moving Cursor to world link".$worldLink->getCssValue("border-bottom");
		$blueBorderColor="3px solid rgb(67, 154, 211)";
		$hoverValue = $worldLink->getCssValue("border-bottom");
		$action->click($worldLink);

		if($hoverValue == $blueBorderColor)
		{
			echo "World Menu has been underlined";
		}
		else
		{
			echo "World Menu has not been underlined";
		}



	}

	public function tearDown()
	{
		//Quit the driver
		$this->webDriver->quit();
	}

	public function performTest()
	{
		$this->setUp();
		$this->verifyWorldMenu();
		$this->tearDown();
	}
}
?>
