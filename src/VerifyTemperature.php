<?php
class MyTest extends PHPUnit_Framework_TestCase
{

	protected $webDriver;

	public function setUp()
	{
		$capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'chrome');
		$this->webDriver = RemoteWebDriver::create("http://localhost:4444/wd/hub", $capabilities);
		$this->webDriver->manage()->window()->maximize();
	}

	public function validateTemperature()
	{
		$this->webDriver->get("https://www.theweathernetwork.com/ca");
		sleep(20);
		$temperatureString = $this->webDriver->findElement(WebDriverBy::xpath("//div[@class='current-location-current-temp-c']"))->getText();
		$temperatureString = substr($temperatureString,0,2);
		if(intval()<15)
		{
			echo "Test has failed";
		}
		else
		{
			echo "Test has passed";
		}
	}

	public function tearDown()
	{
		//Quit the driver
		$this->webDriver->quit();
	}

	public function performTest()
	{
		$this->setUp();
		$this->validateTemperature();
		$this->tearDown();
	}
}
?>
