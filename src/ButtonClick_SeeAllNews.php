<?php
class MyTest extends PHPUnit_Framework_TestCase
{

	protected $webDriver;

	public function setUp()
	{
		$capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'chrome');
		$this->webDriver = RemoteWebDriver::create("http://localhost:4444/wd/hub", $capabilities);
		$this->webDriver->manage()->window()->maximize();
	}

	public function clickbuttonSeeAllNews()
	{
		$this->webDriver->get("https://www.theweathernetwork.com/ca");
		sleep(20);
		$seeAllNewsBtn = $this->webDriver->findElement(WebDriverBy::linkText("See All News"));
		$seeAllNewsBtn->click();
	}

	public function tearDown()
	{
		//Quit the driver
		$this->webDriver->quit();
	}

	public function performTest()
	{
		$this->setUp();
		$this->clickbuttonSeeAllNews();
		$this->tearDown();
	}
}
?>
