<?php
class MyTest extends PHPUnit_Framework_TestCase
{

	protected $webDriver;

	public function setUp()
	{
		$capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'chrome');
		$this->webDriver = RemoteWebDriver::create("http://localhost:4444/wd/hub", $capabilities);
		$this->webDriver->manage()->window()->maximize();
	}

	public function checkLatestMenu()
	{
		$this->webDriver->get("https://www.theweathernetwork.com/ca");
		sleep(20);
		$seeAllNewsBtn = $this->webDriver->findElement(WebDriverBy::linkText("See All News"));
		$seeAllNewsBtn->click();
		sleep(20);
		$latestLink = $this->webDriver->findElement(WebDriverBy::linkText("Latest"));
		$action = new WebDriverActions($this->webDriver);
		$action->moveToElement($latestLink).perform();
		$blueBorderColor="3px solid rgb(67, 154, 211)";
		$hoverValue = $latestLink->getCssValue("border-bottom");

		if($hoverValue == $blueBorderColor)
		{
			echo "Before clicking on Latest, Latest Menu has been underlined";
		}
		else
		{
			echo "Latest Menu has not been underlined";
		}


	}

	public function tearDown()
	{
		//Quit the driver
		$this->webDriver->quit();
	}

	public function performTest()
	{
		$this->setUp();
		$this->checkLatestMenu();
		$this->tearDown();
	}
}
?>
